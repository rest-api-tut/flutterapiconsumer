
import 'dart:convert';

class User {
  num id = 0;
  String firstName = "";
  String lastName = "";
  int age = -1;

  User(this.id, this.firstName, this.lastName, this.age);
  User.fromForm(this.firstName, this.lastName, this.age);
  User.formJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    age = json['age'];
  }

  String toJson() {
    Map<String, dynamic> data = {};
    data['id'] =  id;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['age'] = age;
    return jsonEncode(data);
  }
}