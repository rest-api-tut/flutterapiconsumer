
import 'package:flutter/material.dart';
import 'package:flutter_api_consumer/user.dart';
import 'package:flutter_api_consumer/user_service.dart';

class UserListView extends Container {

  final List<User> users;
  final void Function(User? user) showUser;
  final void Function() updateView;

  UserListView(this.users, this.showUser, this.updateView, {super.key});

  @override
  Widget build(BuildContext context) {
    List<Padding> rows = List.empty(growable: true);

    for(User user in users) {
      rows.add(Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("${user.firstName} ${user.lastName}"),
                Row(
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        var selectedUser = await getUser(user.id);
                        showUser(selectedUser);
                      },
                      child: const Text("show")
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        deleteUser(user.id);
                        showUser(null);
                        updateView();
                      },
                      child: const Text("delete")
                  ),
                ],
              ),
            ],
          ),
      ));
    }
    return Column(children: rows);
  }
}