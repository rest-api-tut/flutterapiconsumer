import 'dart:convert';

import 'package:flutter_api_consumer/user.dart';
import 'package:http/http.dart' as http_client;

const String baseUrl = "http://10.0.2.2:8080/api/users";

getUsers() async {
  var uri = Uri.parse(baseUrl);
  var response = await http_client.get(uri, headers: <String, String>{"Accept": "application/json; charset=UTF-8"});
  List<User> users = List.empty(growable: true);

  var jsonList = jsonDecode(response.body) as List;
  for (var element in jsonList) {
    users.add(User.formJson(element));
  }
  return users;
}

getUser(num id) async {
  var uri = Uri.parse("$baseUrl/$id");
  var response = await http_client.get(uri, headers: <String, String>{"Accept": "application/json; charset=UTF-8"});
  return User.formJson(jsonDecode(response.body));
}

saveUser(User user) async {
  var uri = Uri.parse(baseUrl);
  var response = await http_client.post(uri,
      headers: <String, String>{"Content-Type": "application/json"},
      body: user.toJson()
  );
}

deleteUser(num id) {
  http_client.delete(Uri.parse("$baseUrl/$id"));
}