
import 'package:flutter/material.dart';
import 'package:flutter_api_consumer/user.dart';
import 'package:flutter_api_consumer/user_service.dart';
import 'package:flutter_api_consumer/validators.dart';

class FormContainer extends StatefulWidget {
  final Function() updateView;
  final void Function(User? user) showUser;
  const FormContainer(this.updateView, this.showUser, {Key? key}) : super(key: key);

  @override
  State<FormContainer> createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController ageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            getFormField("first name", firstNameController, firstNameValidator),
            getFormField("last name", lastNameController, lastNameValidator),
            getFormField("age", ageController, ageValidator),
            Padding(
                padding: EdgeInsets.all(10),
                child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: ElevatedButton(

                    child: const Text("Add user"),
                    onPressed: () async {
                      if(_formKey.currentState?.validate() == true) {
                        User user = User.fromForm(
                            firstNameController.text,
                            lastNameController.text,
                            int.parse(ageController.text)
                        );
                        saveUser(user);
                        restForm();
                        widget.showUser(null);
                      }
                    },
                  ),
                ),
            )
          ],
        )
    );
  }

  void restForm() {
    firstNameController.clear();
    lastNameController.clear();
    ageController.clear();
  }

  Padding getFormField(String label, TextEditingController controller, String? Function(String?) validator) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: TextFormField(
          decoration: textFieldDecoration(label),
          controller: controller,
          validator: validator,
        ),
    );
  }

  InputDecoration textFieldDecoration(String label) {
    return InputDecoration(
      label: Text(label),
      hintText: label,
      filled: true,
      fillColor: const Color(0xFFEAEBEE),
      enabledBorder: const OutlineInputBorder(borderSide:  BorderSide(width: 1.0, color: Colors.green))
    );
  }
}
