String? firstNameValidator(String? value) {
  return value == null || value.isEmpty ? "first name cannot be empty" : null;
}

String? lastNameValidator(String? value) {
  return value == null || value.isEmpty ? "last name cannot be empty" : null;
}

String? ageValidator(String? value) {
  return value == null || value.isEmpty || int.parse(value) <= 0 ? "wrong age" : null;
}