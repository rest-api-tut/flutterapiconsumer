import 'package:flutter/material.dart';
import 'package:flutter_api_consumer/form_container.dart';
import 'package:flutter_api_consumer/user.dart';
import 'package:flutter_api_consumer/user_list_view.dart';
import 'package:flutter_api_consumer/user_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter API Consumer',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.green),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter API Consumer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<User> users = List.empty(growable: true);
  User? user;


  _MyHomePageState() {
    updateView();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Column(
        children: getWidgetList(),
      )
    );
  }

  List<Widget> getWidgetList() {
    List<Widget> list = List.of([
      FormContainer(updateView, showUser),
      UserListView(users, showUser, updateView)
    ]);

    if(user != null) {
      list.insert(1, Container(
          height: 80,
          decoration: const BoxDecoration(color: Colors.amberAccent),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("first name: ${user?.firstName}"),
                Text("last name: ${user?.lastName}"),
                Text("age: ${user?.age}")
              ],
            ),
          ),
      ));
    }
    return list;
  }

  void updateView() async {
    var userList = await getUsers();
    setState(() {
      users = userList;
    });
  }

  void showUser(User? user) {
    setState(() {
      this.user = user;
    });
  }
}
